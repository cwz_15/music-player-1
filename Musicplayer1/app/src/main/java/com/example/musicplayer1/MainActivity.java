package com.example.musicplayer1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        populateDataModel();

        connectXMLViews();
        setupRecyclerView();


    }

    void populateDataModel() {
        // Initialize properties of playlist
        playlist.name = "My Playlist";
        playlist.songs = new ArrayList<song>();

        // Create and initialize the first song
        song song = new song();
        song.songName = "Beautiful Dream";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.beautifuldream;

        // Adding the first song to the array of songs in the playlist
        playlist.songs.add(song);

        // Calm

        song = new song();
        song.songName = "cbpd";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.;
        song.mp3Resource = R.raw.song;

        song song = new song();
        song.songName = "Fun and Games";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.funandgames;

        song song = new song();
        song.songName = "Games - worldbeat ";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.gamesworldbeat;

        song song = new song();
        song.songName = "Just Chill";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.justchill;

        song song = new song();
        song.songName = "Latin lovers";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.latinlovers;

        song song = new song();
        song.songName = "Playful";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.playful;

        song song = new song();
        song.songName = "Romantic";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.romantic;

        song song = new song();
        song.songName = "Silent Descent";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.silentdescent;

        // Jazz

        song song = new song();
        song.songName = "All That";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.allthat;

        song song = new song();
        song.songName = "Hip Jazz";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.hipjazz;

        song song = new song();
        song.songName = "Jazz Comedy";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.jazzcomedy;

        song song = new song();
        song.songName = "Jazz Frenchy";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.jazzfrenchy;

        song song = new song();
        song.songName = "Love";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.love;

        song song = new song();
        song.songName = "The Elevator boss a nova";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.theelevatorbossanova;

        song song = new song();
        song.songName = "The Jazz Piano";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.thejazzpiano;

        song song = new song();
        song.songName = "The Lounge";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.thelounge;

        // Pop

        song song = new song();
        song.songName = "Clear Day";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.clearday;

        song song = new song();
        song.songName = "Energy";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.energy;

        song song = new song();
        song.songName = "Funky Element";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.funkyelement;

        song song = new song();
        song.songName = "Hey";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.hey;

        song song = new song();
        song.songName = "Inspire";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.inspire;

        song song = new song();
        song.songName = "Little Idea";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.littleidea;

        song song = new song();
        song.songName = "Sweet";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.sweet;

        // Rap

        song song = new song();
        song.songName = "Rap";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.rap;

        song song = new song();
        song.songName = "Epic";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.epic;

        song song = new song();
        song.songName = "Evolution";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.evolution;

        song song = new song();
        song.songName = "Groovy Hip Hop";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.groovyhiphop;

        song song = new song();
        song.songName = "Thug Life";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.thuglife;

        song song = new song();
        song.songName = "Astrology";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.astrology;

        song song = new song();
        song.songName = "Boulevard";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.boulevard;

        song song = new song();
        song.songName = "Bruh";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.bruh;

        song song = new song();
        song.songName = "Crank it up non stop";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.crankitupnonstop;

        song song = new song();
        song.songName = "Don't you know it's true";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.dontyouknowitstrue;

        song song = new song();
        song.songName = "Dream Land";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.dreamland;

        song song = new song();
        song.songName = "Gotta get it";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.gottagetit;

        song song = new song();
        song.songName = "Greed";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.greed;

        song song = new song();
        song.songName = "It's like that";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.itslikethat;

        song song = new song();
        song.songName = "It's on you";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.itsonyou;

        song song = new song();
        song.songName = "Kk";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.kk;

        song song = new song();
        song.songName = "King paranoia";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.kingparanoia;

        song song = new song();
        song.songName = "Like a loop machine";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.likealoopmachine;

        song song = new song();
        song.songName = "Move your body";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.moveyourbody;

        song song = new song();
        song.songName = "Praise the Lord";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.praisethelord;

        song song = new song();
        song.songName = "Purple";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.purplejs;

        // Workout

        song song = new song();
        song.songName = "A game";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.agame;

        song song = new song();
        song.songName = "Be happy";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.behappy;

        song song = new song();
        song.songName = "Deep urban";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.deepurban;

        song song = new song();
        song.songName = "Disco ain't old school";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.discoaintoldschool;

        song song = new song();
        song.songName = "Driving ambition";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.drivingambition;

        song song = new song();
        song.songName = "Gear";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.gear;

        song song = new song();
        song.songName = "Hiding under";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.hidingunder;

        song song = new song();
        song.songName = "Keeping fit";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.keepingfit;

        song song = new song();
        song.songName = "Minimal techno";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.minimaltechno;

        song song = new song();
        song.songName = "Motivating mornings";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.motivatingmornings;

        song song = new song();
        song.songName = "Positive energy";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.positiveenergy;

        song song = new song();
        song.songName = "Ride the wave";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.ridethewave;

        song song = new song();
        song.songName = "Rising forest";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.risingforest;

        song song = new song();
        song.songName = "Super strong";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.superstrong;

        song song = new song();
        song.songName = "Surfs up dude";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.surfsupdude;

        song song = new song();
        song.songName = "The greatest comeback";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.thegreatestcomeback;

        song song = new song();
        song.songName = "To the next round";
        song.artistName = "bensound.com";
        song.imageResource = R.drawable.image of song;
        song.mp3Resource = R.raw.tothenextround;

        // Rock


    }

    void connectXMLViews();

    songsrecyclerView = findViewById(R.id.song_list_view);

    imageView = findViewById(R.id.imageView);

    TextView = findViewById(R.id.song_name_textview);

    TextView = findViewById(R.id.artist_name_textview);

    imageButton = findViewById(R.id.imageButton);

    imageButton = findViewById(R.id.imageButton2);

    imageButton = findViewById(R.id.imageButton3);

    imageButton = findViewById(R.id.imageButton4);

    void setupRecyclerView();
    LinearLayoutManager layoutManager = new LinearLayoutManager(this);
    songsrecyclerView.setLayoutManager(layoutManger);

    // Connect the adapter to the RecyclerView


}


    // Properties
    playlist playlist = new playlist();

    // XML Views
    RecyclerView recyclerView;
    ImageView imageView;
    TextView songNameView;
    TextView artistNameView;
    ImageButton imageButton;
    ImageButton imageButton2;
    ImageButton imageButton3;
    ImageButton imageButton4;


}